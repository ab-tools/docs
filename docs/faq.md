---
sidebar_position: 3
id: FAQ
---
# Frequently Asked Questions (FAQ)
## How do I use the Calculator?
For all questions regarding the parameters, the plot or the interpretation of the results
check out the [Calculator](Calculator) documentation

## Where can I learn more about Bayesian Statistics?
Some references that I found to be particularly helpful:
- [Will Kurt - "Bayesian Statistics the Fun Way" (2019)](https://nostarch.com/learnbayes)
- [Ben Lambert - "A Students Guide to Bayesian Statistics" (YouTube)](https://www.youtube.com/watch?v=P_og8H-VkIY&list=PLwJRxp3blEvZ8AKMXOy0fc0cqT61GsKCG)


## Is this really free
Yes - the calculator is a free service provided by A-B.Tools.
Please also note that we care deeply about your privacy and thus do not track/log/process any information 
you enter into the calculator. For more details see our Privacy Policy.

## What is an A/B Test?
To evaluate the impact of new features, many websites and apps conduct controlled experiments on their users.
Oftentimes these experiments involve changing some part of the user-interface, such as choosing a different headline or
using different colors (here's one example from [Netflix](https://netflixtechblog.com/what-is-an-a-b-test-b08cc1b57962)).
Crucially however, only some users get to see the updated version while for the rest there is no change at all.
Having separated the users in such a manner, the impact of the change is then evaluated by comparing the behavior of
the two user groups along key-performance indicators such as clicks or conversions.
Finally, a verdict is passed and whichever version works best becomes the new default version and is presented to all users.


## Are those questions really frequently asked?
No. However I don't think they're irrelevant either.
And maybe everyone looking for answers to these questions already found the answer here in which case there is no point in asking.
If you have any more questions feel free get in touch via [e-mail](mailto:hey@a-b.tools)
