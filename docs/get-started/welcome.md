---
sidebar_position: 1
slug: /
---

# Welcome

[A-B.Tools](https://a-b.tools) is a lightweight and open-source A/B testing tool.
It does not matter whether you're running a small webshop, a blog or a large corporation.
If you know how to make a GET-request from your app you can start using a-b.tools!

This documentation is designed to answer all you questions around our product.
If you have any questions feel free to contact us at [hey@a-b.tools](mailto:hey@a-b.tools)

# How A-B.Tools works
## Integrating A-B.Tools into your app
Integrating A-B.Tools into your app is extremely simple: After setting up an experiment, all you need to do is to call 
the appropriate tracking endpoint when a user is enrolled for an experiment and once the target condition has been met.

Just plain HTTP - no additional javascript dependencies, no cookies, no privacy-headaches!

## Analysing your Results
Making sense of data can be challenging and we appreciate not everyone has the statistical know-how to do so.
This is why we have built our calculation engine to be as accessible and human-friendly as possible.
It delivers solid, actionable advice without compromising on statistical rigour.

Not sure what we're talking about? [Try our Free Calculator](https://a-b.tools/calculator) and see for yourself

# Get Started
Ready to improve the digital experience for your product? [Get started by creating your account](/get-started/Register) 




