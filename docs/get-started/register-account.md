---
sidebar_position: 2
id: Register
---

# Account Creation
To use A-B.Tools you need to [create an account](https://a-b.tools/register).
Signing up for an account is completely free and does not require any payment details (e.g. credit card).

## 1. Register your Account
Visit the [Registration Page](https://a-b.tools/register) and provide you e-mail address and desired password.

## 2. Verify your Email Address
We will send you an email to verify your e-mail address. Look for an email with the subject line "Account activation on A-B.Tools". 
The confirmation mail will contain a link directing you to the confirmation page - Hit the "Confirm" button to complete your registration

![Confirmation Page](../../static/img/tutorial/confirmation-page.png)

## 3. Log-In
After completing your registration process proceed to the [Login Page](https://a-b.tools/login) to start using a-b.tools.

Hint: Should you ever forget your password you can reset it [here](https://a-b.tools/forgot-password)
