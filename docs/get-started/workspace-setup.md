---
sidebar_position: 3
id: Workspace-Setup
---

# Workspace Setup
## Create new Workspace
After creating an account you will need to create a new workspace.
A workspace typically represents one company and is the central repository for your experiments.
While you can create and join more than one workspace, having one is usually enough.

To create a new workspace select the "New Workspace" button in your Workspaces overview page.

![Workspace List](../../static/img/tutorial/workspace-list.png)

## Customize Workspace
After creating your workspace you will be able to customize your workspace to your liking.
All items can be changed later so don't worry too much about things like the name or the description.
You should however give some thought to your workspace slug.

### Workspace Slug
Your workspace slug is a short and globally unique identifier that is used as the primary public reference to your workspace.
Since all your experiments' tracking endpoints will later reference this slug (see also [Setting up an Experiment](Experiment-Setup) )
it is usually a good idea to change this value to something more memorable.

:::caution

While you can change your workspace's slug at any time,
please be aware that this will also change the tracking endpoints for your experiments. 
Thus, you need to ensure all references to your experiments' endpoints reflect that change!

:::
![Edit Workspace](../../static/img/tutorial/workspace-edit.png)
