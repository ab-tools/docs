---
sidebar_position: 5
id: Pricing
---
# Pricing

## Pricing in a Nushell
- A-B.Tools follows a tier-based pricing model based on traffic-volume
- There are no costs associated with user accounts - charges are only levied on workspaces
- Each workspace comes with a free 30-day trial period
- There are no limits to the number of experiments you can run per workspace
- No payment info is required for creating a user account or workspace. 
Payment details are only required if you wish to continue using a workspace after the 30-day trial period

## Pricing Table

| Events / month | Price (EUR) / month |
|------------------|-------------|
| < 10.000               | 6           |
| < 100.000              | 12          |
| < 200.000              | 20          |
| < 500.000            | 30          |
| < 1.000.000             | 50          |
| < 2.000.000             | 70          |
| < 5.000.000             | 100         |
| < 10.000.000            | 150         |



