---
sidebar_position: 5
id: Glossary
---
# Glossary
## ROPE
_Region of Practical Equivalence_ - think of this as the minimum detectable effect you care about.
Example: You're running an experiment to establish if your new landing page increases the sign-up rate.
Since you're only willing to fully commit to the new version if it outperforms the old one by at least 2 percent you set ROPE to equal 2 (%).
For further details see section "When do I stop my experiment"

## HDI
_Highest Density Interval_ - think of this as the range of the most credible values for your parameter.
Technically, it is marked by the the interval of the probability function holding 95% of your mass.
If you're familiar with (frequentist) statistics, this figure is somewhat comparable to a significance level.
