// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'A-B Tools Documentation',
  tagline: 'Dinosaurs are cool',
  url: 'https://docs.a-b.tools',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'a-b.tools', // Usually your GitHub org/user name.
  projectName: 'docs', // Usually your repo name.

  presets: [
    [
      '@docusaurus/preset-classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          routeBasePath: '/',
          // Please change this to your repo.
          editUrl: 'https://github.com/facebook/docusaurus/edit/main/website/',
        },
        blog:false,
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: 'A-B Tools',
        logo: {
          alt: 'a-b.tools logo',
          src: 'img/logo.svg',
          href: 'https://a-b.tools',
        },
        items: [
          {
            type: 'doc',
            docId: 'FAQ',
            position: 'left',
            label: 'Docs',
          },
          {
            href: 'https://a-b.tools',
            label: 'A-B.tools',
            position: 'right',
          },
          {
            href: 'https://gitlab.com/ab-tools/docs',
            label: 'GitLab',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [

        ],
        copyright: `Copyright © ${new Date().getFullYear()} a-b.tools`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = {
  ...config,
  scripts: [{src: 'https://plausible.labeljet.net/js/plausible.js', defer: true, 'data-domain': 'docs.a-b.tools'}],
}
